import * as React from 'react';
import { Component } from 'react-simplified';
import ReactDOM from 'react-dom';
import { NavLink, HashRouter, Route } from 'react-router-dom';
import { pool } from './mysql-pool';

class Menu extends Component {
  render() {
    return (
      <div>
        <NavLink exact to="/" activeStyle={{ color: 'darkblue' }}>
          StudAdm
        </NavLink>
        {' ' /* Add extra space between menu items */}
        <NavLink to="/students" activeStyle={{ color: 'darkblue' }}>
          Students
        </NavLink>
        {' ' /* Add extra space between menu items */}
        <NavLink to="/programs" activeStyle={{ color: 'darkblue' }}>
          Studieprogram
        </NavLink>
      </div>
    );
  }
}

class Home extends Component {
  render() {
    return <div>Welcome to StudAdm</div>;
  }
}

class StudentList extends Component {
  students = [];

  render() {
    return (
      <ul>
        {this.students.map((student) => (
          <li key={student.id}>
            <NavLink to={'/students/' + student.id}>{student.name}</NavLink>
          </li>
        ))}
      </ul>
    );
  }

  mounted() {
    pool.query('SELECT id, name FROM Students', (error, results) => {
      if (error) return console.error(error); // If error, show error in console (in red text) and return

      this.students = results;
    });
  }
}

class StudentDetails extends Component {
  student = null;

  render() {
    if (!this.student) return null;

    return (
      <ul>
        <li>Name: {this.student.name}</li>
        <li>Email: {this.student.email}</li>
        <li>Program: {this.student.programname}</li>
      </ul>
    );
  }

  mounted() {
    pool.query(
      'SELECT s.name, s.email, p.name as programname FROM Students as s LEFT JOIN Programs p ON s.program=p.id WHERE s.id=?',
      [this.props.match.params.id],
      (error, results) => {
        if (error) return console.error(error); // If error, show error in console (in red text) and return

        this.student = results[0];
      }
    );
  }
}

class ProgramList extends Component {
  programs = [];

  render() {
    return (
      <ul>
        {this.programs.map((program) => (
          <li key={program.id}>
            <NavLink to={'/programs/' + program.id}>{program.name}</NavLink>
          </li>
        ))}
      </ul>
    );
  }

  mounted() {
    pool.query('SELECT id, name FROM Programs', (error, results) => {
      if (error) return console.error(error);

      this.programs = results;
    });
  }
}

class ProgramDetails extends Component {
  program = null;
  programStudents = [];
  render() {
    if (!this.program) return null;

    return (
      <ul>
        <li>Name: {this.program[0].name}</li>
        <li>Code: {this.program[0].code}</li>
        <li>
          Students:
          <ul>
            {this.program.map((student) => (
              <li key={'student' + student.id}>
                <NavLink to={'/students/' + student.id}>{student.studentname}</NavLink>
              </li>
            ))}
          </ul>
        </li>
      </ul>
    );
  }

  mounted() {
    pool.query(
      'SELECT s.name as studentname, p.name, p.code, s.id FROM Students as s LEFT JOIN Programs p ON p.id=s.program WHERE p.id=?',
      [this.props.match.params.id],
      (error, results) => {
        if (error) return console.error(error); // If error, show error in console (in red text) and return

        this.program = results;
      }
    );
  }
}

ReactDOM.render(
  <HashRouter>
    <div>
      <Menu />
      <Route exact path="/" component={Home} />
      <Route path="/students" component={StudentList} />
      <Route exact path="/students/:id" component={StudentDetails} />
      <Route path="/programs" component={ProgramList} />
      <Route exact path="/programs/:id" component={ProgramDetails} />
    </div>
  </HashRouter>,
  document.getElementById('root')
);
